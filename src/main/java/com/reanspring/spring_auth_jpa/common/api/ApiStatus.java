package com.reanspring.spring_auth_jpa.common.api;

import com.reanspring.spring_auth_jpa.common.StatusCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiStatus {
    private int code;
    private String message;

    public ApiStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApiStatus(StatusCode statusCode){
        this.code = statusCode.getCode();
        this.message = statusCode.getMessage();
    }
}
