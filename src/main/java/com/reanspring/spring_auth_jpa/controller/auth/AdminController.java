package com.reanspring.spring_auth_jpa.controller.auth;


import com.reanspring.spring_auth_jpa.controller.AbstractRestController;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/admin")
public class AdminController extends AbstractRestController {



}
