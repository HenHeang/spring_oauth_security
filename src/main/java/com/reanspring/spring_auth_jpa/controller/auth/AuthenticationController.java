package com.reanspring.spring_auth_jpa.controller.auth;

import com.reanspring.spring_auth_jpa.controller.AbstractRestController;
import com.reanspring.spring_auth_jpa.payload.auth.LoginRequest;
import com.reanspring.spring_auth_jpa.payload.auth.LoginResponse;
import com.reanspring.spring_auth_jpa.payload.auth.RegisterRequest;
import com.reanspring.spring_auth_jpa.service.auth.AuthenticationService;
import com.reanspring.spring_auth_jpa.service.otp.OTPService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController extends AbstractRestController {
    private final AuthenticationService authenticationService;
    private final OTPService otpService;

    @PostMapping("/register")
    public ResponseEntity<LoginResponse> register(
            @RequestBody RegisterRequest request
    ) {
        return ResponseEntity.ok(authenticationService.register(request));
    }

   @PostMapping("/login")
    public ResponseEntity<LoginResponse> authenticate(
            @RequestBody LoginRequest request
    ) {
        return ResponseEntity.ok(authenticationService.login(request));
    }

    @PostMapping("/generatePinCode")
    public ResponseEntity<?> generatePinCode(@RequestParam String email){
        otpService.generatePinCode("henheang15@gmail.com");
        return ok(otpService.generatePinCode(email));
    }
}
