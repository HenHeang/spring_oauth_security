package com.reanspring.spring_auth_jpa.exception;

import com.reanspring.spring_auth_jpa.common.StatusCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessException extends RuntimeException{
    private Object body;
    private final StatusCode errorCode;

    public BusinessException(StatusCode errorCode, Object body) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
        this.body = body;
    }

    public BusinessException(StatusCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public BusinessException(StatusCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public BusinessException(StatusCode errorCode, Throwable e) {
        this(errorCode);
    }
}
