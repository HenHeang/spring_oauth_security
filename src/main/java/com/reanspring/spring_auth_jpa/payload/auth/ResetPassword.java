package com.reanspring.spring_auth_jpa.payload.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResetPassword {
    private String email;
    private String newPassword;
    private String confirmPassword;
}
