package com.reanspring.spring_auth_jpa.service.auth;

import com.reanspring.spring_auth_jpa.domain.user.User;
import com.reanspring.spring_auth_jpa.payload.auth.LoginRequest;
import com.reanspring.spring_auth_jpa.payload.auth.LoginResponse;
import com.reanspring.spring_auth_jpa.payload.auth.RegisterRequest;

public interface AuthenticationService {
    LoginResponse register(RegisterRequest request);

    void saveUserToken(User user, String jwtToken);

    LoginResponse login(LoginRequest request);

    void revokeAllUserTokens(User user);
}
