package com.reanspring.spring_auth_jpa.service.auth;

import com.reanspring.spring_auth_jpa.common.StatusCode;
import com.reanspring.spring_auth_jpa.domain.user.User;
import com.reanspring.spring_auth_jpa.domain.user.UserRepository;
import com.reanspring.spring_auth_jpa.exception.BusinessException;
import com.reanspring.spring_auth_jpa.payload.auth.LoginRequest;
import com.reanspring.spring_auth_jpa.payload.auth.LoginResponse;
import com.reanspring.spring_auth_jpa.payload.auth.RegisterRequest;
import com.reanspring.spring_auth_jpa.security.config.JwtService;
import com.reanspring.spring_auth_jpa.security.token.Token;
import com.reanspring.spring_auth_jpa.security.token.TokenRepository;
import com.reanspring.spring_auth_jpa.security.token.TokenType;
import com.reanspring.spring_auth_jpa.security.user.Role;
import com.reanspring.spring_auth_jpa.utils.EmailUtils;
import com.reanspring.spring_auth_jpa.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserRepository repository;
    private final TokenRepository tokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    @Override
    public LoginResponse register(RegisterRequest request) {
//        Check before register
        if (request.getEmail() == null || StringUtils.isBlank(request.getEmail())) {
            throw new BusinessException(StatusCode.EMAIL_IS_NUll);
        } else if (!EmailUtils.isEmail(request.getEmail())) {
            throw new BusinessException(StatusCode.INVALID_EMAIL_FORMAT);
        } else if (repository.findByEmail(request.getEmail()).isPresent()) {
            throw new BusinessException(StatusCode.EMAIL_EXIST);
        }

        var user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .build();
        var savedUser = repository.save(user);
        var token = jwtService.generateToken(savedUser);
        var refreshToken = jwtService.generateRefreshToken(savedUser);
        saveUserToken(savedUser, token);
        return LoginResponse.builder()
                .accessToken(token)
                .refreshToken(refreshToken)
                .build();
    }

    @Override
    public void saveUserToken(User user, String jwtToken) {
        var token = Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepository.save(token);
    }

    @Override
    public LoginResponse login(LoginRequest request) {
        var user = repository.findByEmail(request.getEmail())
                .orElseThrow(() -> new BusinessException(StatusCode.EMAIL_NOT_FOUND));

        // login validation
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getEmail(),
                            request.getPassword()
                    )
            );
        } catch (BadCredentialsException e) {
            throw new BusinessException(StatusCode.BAD_CREDENTIALS);
        }
        catch (AuthenticationException ex) {
            // Handle other authentication exceptions
            throw new BusinessException(StatusCode.AUTHENTICATION_FAILED, ex.getMessage());
        }
        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);
        return LoginResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }

    @Override
    public void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

}
