package com.reanspring.spring_auth_jpa.service.otp;


import com.reanspring.spring_auth_jpa.payload.pincode.PinCodeRequest;

public interface OTPService{

    PinCodeRequest generatePinCode(String email);
}
