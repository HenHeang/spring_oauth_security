package com.reanspring.spring_auth_jpa.service.otp;

import com.reanspring.spring_auth_jpa.common.StatusCode;
import com.reanspring.spring_auth_jpa.domain.user.User;
import com.reanspring.spring_auth_jpa.domain.user.UserRepository;
import com.reanspring.spring_auth_jpa.exception.BusinessException;
import com.reanspring.spring_auth_jpa.payload.pincode.PinCodeRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class OTPServiceImpl implements OTPService {
    private final SendMailOTPService sendMailOTPService;
    private final UserRepository userRepository;

    @Override
    public PinCodeRequest generatePinCode(String email) {
        Random random  = new Random();
        int pinCode = random.nextInt(900000)+100000;
        String code = String.valueOf(pinCode);
        PinCodeRequest pinCodeRequest = new PinCodeRequest();
        pinCodeRequest.setEmail(email);
        pinCodeRequest.setPinCode(code);

        User user = userRepository.findByEmail(email).orElseThrow(() -> new BusinessException(StatusCode.EMAIL_NOT_FOUND));
        user.setPinCode(code);
        user.setPinCodeGenerateTime(String.valueOf(LocalDateTime.now()));
        userRepository.save(user);

        sendMailOTPService.sendMailOTP(pinCodeRequest);


        return pinCodeRequest;
    }

}
