package com.reanspring.spring_auth_jpa.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.http.HttpMethod;

import java.util.Map;
import java.util.stream.Collectors;

public class ObjectUtils {
    static private ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.registerModule(new JavaTimeModule());


    }

    public static String writeValueAsString(Object value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String convertPojoToMap(Object pojo) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(pojo, new TypeReference<Map<String, Object>>() {
        });
        return map.entrySet().stream().map(x -> {
            return x.getKey() + "=" + x.getValue();
        }).collect(Collectors.joining("&"));

        // Convert Map to POJO
        // Foo anotherFoo = mapper.convertValue(map, Foo.class);
    }

    public static <T> T readValue(String str, TypeReference<T> tr) {
        try {
            return mapper.readValue(str, tr);
        } catch (Exception e) {
            return null;
        }
    }
    public static String writeValueAsSingleLineString(Object value){
        try {
            mapper.disable(SerializationFeature.INDENT_OUTPUT);
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
        }

        return String.valueOf(value);
    }

    public static StringBuilder legBeforeRequest(String url, HttpMethod httpMethod, Object request){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Request URL: ").append(url).append("\n");
        stringBuilder.append("Request Method: ").append(httpMethod).append("\n");
        stringBuilder.append("Request Body: ").append(writeValueAsSingleLineString(request)).append("\n");
        return stringBuilder;
    }

    public StringBuilder legAfterRequest(String url, HttpMethod httpMethod, Object request, Object response){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Request URL: ").append(url).append("\n");
        stringBuilder.append("Request Method: ").append(httpMethod).append("\n");
        stringBuilder.append("Request Body: ").append(writeValueAsSingleLineString(request)).append("\n");
        stringBuilder.append("Response Body: ").append(writeValueAsSingleLineString(response)).append("\n");
        return stringBuilder;
    }
}
